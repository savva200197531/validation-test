const arrow = document.querySelector('.lil-arrow-checkbox');
const arrowPng = document.querySelector('.lil-arrow');
const dropdownEl = document.querySelector('.dropdown-el');
const buttons = document.querySelector('.buttons');

const radioDollar = document.querySelector('.dollar');
const radioPercent = document.querySelector('.percent');

const profitDiv = document.querySelector('.profit-div');
const lesionDiv = document.querySelector('.lesion-div');

const profitCheckbox = document.querySelector('.profit-checkbox');
const profitInput = document.querySelector('.profit-input');
const lesionCheckbox = document.querySelector('.lesion-checkbox');
const lesionInput = document.querySelector('.lesion-input');

const investSum = document.querySelector('.invest-sum-input');
const investSumDiv = document.querySelector('.invest-sum-div');
const multiplyInput = document.querySelector('.multiply-input');
const multiplyDiv = document.querySelector('.multiply-div');
const multiply = document.querySelector('.multiply');
const actualSum = document.querySelector('.actual-sum');

const radioButtons = document.getElementsByName('type');
const radioButtonsArr = Array.prototype.slice.call(radioButtons);

const noValidTemplate = document.querySelector('#no-valid');
const noValidBg = noValidTemplate.content.querySelector('.no-valid-bg');
const noValid100 = noValidTemplate.content.querySelector('.no-valid100');
const noValid200000 = noValidTemplate.content.querySelector('.no-valid200000');

const noValidMultiplyTemplate = document.querySelector('#no-valid-multiply');
const noValidMultiplyBg = noValidMultiplyTemplate.content.querySelector('.no-valid-multiply-bg');
const noValidMultiply1 = noValidMultiplyTemplate.content.querySelector('.no-valid-multiply1');
const noValidMultiply40 = noValidMultiplyTemplate.content.querySelector('.no-valid-multiply40');

const rangeTemplate = document.querySelector('#range');
const rangeDiv = rangeTemplate.content.querySelector('.range-div');
const rangeInput = rangeTemplate.content.querySelector('.range-container-input');


const upButton = document.querySelector('.growth');
const downButton = document.querySelector('.reduction');

function dropdown() {
  if (arrow.checked === false) {
    dropdownEl.classList.add('hidden');
    arrowPng.style = `transform: rotateZ(-90deg); transition: 0.3s;`;
    buttons.style.marginTop = '135px'
  } else {
    dropdownEl.classList.remove('hidden');
    arrowPng.style = `transform: rotateZ(0deg); transition: 0.3s;`;
    buttons.style.marginTop = '21px'
  }
}

arrow.addEventListener('click', dropdown);


actualSum.innerHTML = `= $ ${ investSum.value * multiplyInput.value }`;

function dependence() {
  actualSum.innerHTML = `= $ ${ investSum.value * multiplyInput.value }`;
}

function actualSumCheck() {
  if (investSum.value.length > 5) {
    actualSum.style.fontSize = `11px`
  } else {
    actualSum.style.fontSize = `13px`
  }
}

function lesionCheck() {
  if (lesionCheckbox.checked === false) {
    lesionInput.value = ''
  }
}


function profitCheck() {
  if (profitCheckbox.checked === false) {
    profitInput.value = ''
  }
}

function profitValueWriter() {
  if (radioDollar.checked === true && profitCheckbox.checked === true) {
    profitInput.value = Math.floor(investSum.value * 0.3);
  } else if (radioPercent.checked === true && profitCheckbox.checked === true) {
    profitInput.value = Math.floor((investSum.value * 0.3) / investSum.value * 100);
  }
}

function lesionInputWriter() {
  if (radioDollar.checked === true && lesionCheckbox.checked === true) {
    lesionInput.value = Math.floor(investSum.value * 0.3);
  } else if (radioPercent.checked === true && lesionCheckbox.checked === true) {
    lesionInput.value = Math.floor((investSum.value * 0.3) / investSum.value * 100);
  }
}


function checkRadio() {
  if (this.classList.value === 'dollar') {
    profitDiv.classList.add('two');
    profitDiv.classList.remove('one');
    lesionDiv.classList.add('two');
    lesionDiv.classList.remove('one');
  } else {
    profitDiv.classList.remove('two');
    profitDiv.classList.add('one');
    lesionDiv.classList.remove('two');
    lesionDiv.classList.add('one');

  }
}

radioButtonsArr.forEach(radio => {
  radio.addEventListener('click', checkRadio);
  radio.addEventListener('click', profitValueWriter);
  radio.addEventListener('click', lesionInputWriter);
});

investSum.addEventListener('input', profitValueWriter);
investSum.addEventListener('input', lesionInputWriter);
investSum.addEventListener('input', actualSumCheck);
profitCheckbox.addEventListener('click', profitValueWriter);
lesionCheckbox.addEventListener('click', lesionInputWriter);
profitCheckbox.addEventListener('click', profitCheck);
lesionCheckbox.addEventListener('click', lesionCheck);


investSum.addEventListener('input', dependence);


multiplyInput.addEventListener('input', dependence);


investSumDiv.appendChild(noValidBg);
multiplyDiv.appendChild(noValidMultiplyBg);


multiply.appendChild(rangeDiv);

function rangeChange() {
  multiplyInput.value = rangeInput.value;
}

rangeInput.addEventListener('input', rangeChange);
rangeInput.addEventListener('input', dependence);


function validate(evt) {
  const theEvent = evt || window.event;
  let key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode(key);
  const regex = /[^0-9]/;
  if (regex.test(key)) {
    theEvent.returnValue = false;
    if (theEvent.preventDefault) theEvent.preventDefault();
  }
}

investSum.addEventListener('keypress', validate);
multiplyInput.addEventListener('keypress', validate);


function inputValidate() {
  const invest = investSum.value;
  const multiply = multiplyInput.value;
  if (this.classList.value ? 'invest-sum-input' : 'invest-sum-input red-border') {
    if (invest < 100) {
      noValid100.classList.remove('hidden');
      noValidBg.classList.remove('hidden');
      investSum.classList.add('red-border');
    } else if (invest > 200000) {
      noValid200000.classList.remove('hidden');
      noValidBg.classList.remove('hidden');
      investSum.classList.add('red-border');
    } else {
      noValid100.classList.add('hidden');
      noValid200000.classList.add('hidden');
      noValidBg.classList.add('hidden');
      investSum.classList.remove('red-border');
    }
  }
  if (this.classList.value ? 'multiply-input' : 'multiply-input red-border') {
    if (multiply < 1) {
      noValidMultiply1.classList.remove('hidden');
      noValidMultiplyBg.classList.remove('hidden');
      multiplyInput.classList.add('red-border');
    } else if (multiply > 40) {
      noValidMultiply40.classList.remove('hidden');
      noValidMultiplyBg.classList.remove('hidden');
      multiplyInput.classList.add('red-border');
    } else {
      noValidMultiply1.classList.add('hidden');
      noValidMultiply40.classList.add('hidden');
      noValidMultiplyBg.classList.add('hidden');
      multiplyInput.classList.remove('red-border');
    }
  }
}

investSum.addEventListener('input', inputValidate);
multiplyInput.addEventListener('input', inputValidate);
rangeInput.addEventListener('input', inputValidate);


multiplyInput.addEventListener('click', function () {
  if (rangeDiv.classList.value === 'range-div hidden') {
    rangeDiv.classList.remove('hidden');
  } else {
    rangeDiv.classList.add('hidden');
  }
});


function dataSet(event) {
  const sumInv = investSum.value;
  const mult = multiplyInput.value;
  let takeProfit;
  let stopLoss;

  if (sumInv >= 100 && sumInv <= 200000 && mult >= 1 && mult <= 40) {
    radioButtonsArr.forEach((event) => {
      if (event.classList.value === 'dollar' && event.checked === true) {
        takeProfit = stopLoss = Math.floor(sumInv * 0.3);
        if (takeProfit >= sumInv * 0.1 && stopLoss >= sumInv * 0.1 && stopLoss <= sumInv) {
          if (profitCheckbox.checked === true && lesionCheckbox.checked === true) {
            if (this.classList.value === 'growth') {
              xhr.send(`direction=growth&SumInv=${ sumInv }&mult=${ mult }&takeProfit=${ takeProfit }&stopLoss=${ stopLoss }`);
            } else if (this.classList.value === 'reduction') {
              xhr.send(`direction=reduction&SumInv=${ sumInv }&mult=${ mult }&takeProfit=${ takeProfit }&stopLoss=${ stopLoss }`);
            }
          } else if (lesionCheckbox.checked === true) {

            if (this.classList.value === 'growth') {
              xhr.send(`direction=growth&SumInv=${ sumInv }&mult=${ mult }&stopLoss=${ stopLoss }`);
            } else if (this.classList.value === 'reduction') {
              xhr.send(`direction=reduction&SumInv=${ sumInv }&mult=${ mult }&stopLoss=${ stopLoss }`);
            }
          } else if (profitCheckbox.checked === true) {
            if (this.classList.value === 'growth') {
              xhr.send(`direction=growth&SumInv=${ sumInv }&mult=${ mult }&takeProfit=${ takeProfit }`);
            } else if (this.classList.value === 'reduction') {
              xhr.send(`direction=reduction&SumInv=${ sumInv }&mult=${ mult }&takeProfit=${ takeProfit }`);
            }
          } else {
            if (this.classList.value === 'growth') {
              xhr.send(`direction=growth&SumInv=${ sumInv }&mult=${ mult }`);
            } else if (this.classList.value === 'reduction') {
              xhr.send(`direction=reduction&SumInv=${ sumInv }&mult=${ mult }`);
            }
          }
        } else {
          if (takeProfit < sumInv * 0.1) {
            console.log(`Значение прибыли не может быть меньше ${ sumInv * 0.1 }`);
          }
          if (stopLoss < sumInv * 0.1) {
            console.log(`Значение убытка не может быть меньше ${ sumInv * 0.1 }`);
          }
          if (stopLoss > sumInv) {
            console.log(`Значение убытка не может быть больше ${ sumInv }`);
          }
        }
      } else if (event.classList.value === 'percent' && event.checked === true) {

        takeProfit = stopLoss = Math.floor(sumInv * 0.3) / sumInv * 100;

        if (takeProfit >= (sumInv * 0.1) / sumInv * 100 && stopLoss >= (sumInv * 0.1) / sumInv * 100 && stopLoss <= sumInv) {
          if (profitCheckbox.checked === true && lesionCheckbox.checked === true) {
            if (this.classList.value === 'growth') {
              xhr.send(`direction=growth&SumInv=${ sumInv }&mult=${ mult }&takeProfit=${ Math.floor(sumInv * 0.3) }&stopLoss=${ Math.floor(sumInv * 0.3) }`);
            } else if (this.classList.value === 'reduction') {
              xhr.send(`direction=reduction&SumInv=${ sumInv }&mult=${ mult }&takeProfit=${ Math.floor(sumInv * 0.3) }&stopLoss=${ Math.floor(sumInv * 0.3) }`);
            }
          } else if (lesionCheckbox.checked === true) {

            if (this.classList.value === 'growth') {
              xhr.send(`direction=growth&SumInv=${ sumInv }&mult=${ mult }&stopLoss=${ Math.floor(sumInv * 0.3) }`);
            } else if (this.classList.value === 'reduction') {
              xhr.send(`direction=reduction&SumInv=${ sumInv }&mult=${ mult }&stopLoss=${ Math.floor(sumInv * 0.3) }`);
            }
          } else if (profitCheckbox.checked === true) {
            if (this.classList.value === 'growth') {
              xhr.send(`direction=growth&SumInv=${ sumInv }&mult=${ mult }&takeProfit=${ Math.floor(sumInv * 0.3) }`);
            } else if (this.classList.value === 'reduction') {
              xhr.send(`direction=reduction&SumInv=${ sumInv }&mult=${ mult }&takeProfit=${ Math.floor(sumInv * 0.3) }`);
            }
          } else {
            if (this.classList.value === 'growth') {
              xhr.send(`direction=growth&SumInv=${ sumInv }&mult=${ mult }`);
            } else if (this.classList.value === 'reduction') {
              xhr.send(`direction=reduction&SumInv=${ sumInv }&mult=${ mult }`);
            }
          }
        } else {
          if (takeProfit < (sumInv * 0.1) / sumInv * 100) {
            console.log('Значение прибыли не может быть меньше 10%')
          }
          if (stopLoss < (sumInv * 0.1) / sumInv * 100) {
            console.log('Значение убытка не может быть меньше 10%')
          }
          if (stopLoss > sumInv) {
            console.log('Значение убытка не может быть больше 100%')
          }
        }
      }
    })
  } else {
    if (sumInv < 100) {
      noValid100.classList.remove('hidden');
      noValidBg.classList.remove('hidden');
      investSum.classList.add('red-border');
    }
    if (sumInv > 200000) {
      noValid200000.classList.remove('hidden');
      noValidBg.classList.remove('hidden');
      investSum.classList.add('red-border');
    }
    if (mult < 1) {
      noValidMultiply1.classList.remove('hidden');
      noValidMultiplyBg.classList.remove('hidden');
      multiplyInput.classList.add('red-border');
    }
    if (mult > 40) {
      noValidMultiply40.classList.remove('hidden');
      noValidMultiplyBg.classList.remove('hidden');
      multiplyInput.classList.add('red-border');
    }
  }
}

downButton.addEventListener('click', dataSet);
upButton.addEventListener('click', dataSet);


let xhr = new XMLHttpRequest();
xhr.open("POST", 'http://test.com/test', true);

//Передает правильный заголовок в запросе
xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

xhr.onreadystatechange = function () {//Вызывает функцию при смене состояния.
  if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
    // Запрос завершен. Здесь можно обрабатывать результат.
  }
};

















